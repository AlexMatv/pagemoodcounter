package gui.model;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.scene.chart.LineChart;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;

import static sun.text.normalizer.UTF16.append;

public class Website {

    private StringProperty url;
    private LinkedHashMap<String, ArrayList<Integer>> threads;
    private ArrayList<String> threadUrls;

    public Website(String url){
        this.url = new SimpleStringProperty(url);
        this.threads = new LinkedHashMap<>();
        this.threadUrls = new ArrayList<>();
    }


    public String getUrl(){
        return url.get();
    }

    public void setUrl(String url){
        this.url.set(url);
    }

    public void addThreadurl(String url) {
        this.threadUrls.add(url);
    }

    public LinkedHashMap<String, ArrayList<Integer>> getThreads () {
        return this.threads;
    }

    public ArrayList<String> getThreadUrls() {
        return threadUrls;
    }

    public void setThreads(LinkedHashMap<String, ArrayList<Integer>> threads){
        this.threads = threads;
    }

    /*for(String key : this.threads.keySet()) {
        // ArrayList<Integer> value = this.threads.get(key);
        for (String key1 : threads.keySet()) {
            if (key == key1) {
                ArrayList<Integer> arrayLTemp = threads.get(key1);
                this.threads.get(key).add(arrayLTemp.get(arrayLTemp.size() - 1));
            }
        }
    }*/

   public ArrayList<Integer> calculateAverage(){
       ArrayList<Integer> averageScores = new ArrayList<>();
       int numberOfScores = 1;
       ArrayList<Integer> sum = new ArrayList<>();
           for (String key : threads.keySet()) {
               ArrayList<Integer> scores = threads.get(key);
               if (!(scores.isEmpty())) {
                   numberOfScores = scores.size();
                   for (int i = 0; i <= (numberOfScores - 1); i++) {
                       if (!(sum.isEmpty()) && (sum.size() > i)) {
                           sum.set(i, (sum.get(i) + scores.get(i)));
                       } else {
                           sum.add(scores.get(i));
                       }
                   }
               }
           }

       for (Integer score : sum) {
           averageScores.add(sum.indexOf(score), (score / (threads.size())));
       }
       return averageScores;
   }
}
