package gui.model;

import javafx.beans.property.*;

public class DiscussionG {

    private StringProperty url;
    private IntegerProperty score;

    public DiscussionG(String url, Integer score){
        this.url = new SimpleStringProperty(url);
        this.score = new SimpleIntegerProperty(score);
    }

    public String getUrl(){
        return url.get();
    }

    public void setUrl(String url){
        this.url.set(url);
    }

    public void setScore(Integer score){
        this.score.set(score);
    }

    public StringProperty urlProperty() {
        return url;
    }

    public IntegerProperty scoreProperty() {
        return score;
    }

/*    public Integer getThreads(){
        return score.get();
    }


    public IntegerProperty getScore() {
        return score;
    }*/
}
