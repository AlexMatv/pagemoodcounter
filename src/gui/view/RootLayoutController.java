package gui.view;

import gui.Main;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;

public class RootLayoutController {

    private Main mainApp;

    public void setMain(Main mainApp) {
        this.mainApp = mainApp;
    }

    @FXML
    private void handleWebsiteAdd() {
        mainApp.showWebsiteAddWindow();
    }

    @FXML
    private void handleThreadAdd() {
        mainApp.showThreadAddWindow();
    }


    @FXML
    private void handleStartEngine() {
        mainApp.startEngine();
    }

    @FXML
    private void handleStopEngine() {
        mainApp.stopEngine();
    }

    @FXML
    private void handleAbout() {
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("Help");
        alert.setHeaderText("Help text here");
        alert.setContentText("More text here");

        alert.showAndWait();
    }

    @FXML
    private void handleExit() {
        System.exit(0);
    }

    @FXML
    private void handThreadList() {
        mainApp.showThreadList();
    }

    @FXML
    private void handleShowMoodGraph() {
        mainApp.showMoodGraph();
    }

    @FXML
    private void handleShowSettingsWindow() {
        mainApp.showSettingsWindow();
    }
}
