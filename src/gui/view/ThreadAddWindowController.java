package gui.view;

import gui.Main;
import gui.model.Website;
import javafx.fxml.FXML;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

public class ThreadAddWindowController {

    @FXML
    private TextField threadUrlField;

    @FXML
    private ChoiceBox chooseWebsiteMenu = new ChoiceBox();

    private Main mainApp;

    private Stage threadAddStage;
    //private Website website;
    private boolean addClicked = false;

    @FXML
    private void initialize(){
        for(Website site : Main.websitesData) {
            chooseWebsiteMenu.getItems().add(site.getUrl());
        }
    }

    public void setThreadAddStage(Stage threadAddStage){
        this.threadAddStage = threadAddStage;
    }

    public void setMainApp(Main mainApp) {
        this.mainApp = mainApp;
    }

    public boolean isAddClicked() {
        return addClicked;
    }

    @FXML
    private void handleAdd(){
        System.out.println(chooseWebsiteMenu.getValue());
        System.out.println(threadUrlField.getText());
        mainApp.addThread((String) chooseWebsiteMenu.getValue(), threadUrlField.getText());
        threadUrlField.clear();
    }

    @FXML
    private void handleCancel() {
        threadAddStage.close();
    }
}
