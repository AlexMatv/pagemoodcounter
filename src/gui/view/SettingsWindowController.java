package gui.view;

import gui.Main;
import javafx.fxml.FXML;
import javafx.scene.control.Slider;
import javafx.stage.Stage;

public class SettingsWindowController {
    @FXML
    private Slider intervalSlider;

    private Main mainApp;

    private Stage settingsStage;

    private boolean okClicked = false;

    @FXML
    private void initialize(){
        this.intervalSlider.setValue(Main.checkingInterval);
    }

    public void setSettingsStage(Stage settingsStage){
        this.settingsStage = settingsStage;
    }

    public void setMainApp(Main mainApp) {
        this.mainApp = mainApp;
    }

   public boolean isOkClicked() {
       return okClicked;
   }

    @FXML
    private void handleOk(){
        Double interval = intervalSlider.getValue();
        mainApp.setInterval(interval.intValue());
        settingsStage.close();
    }

    @FXML
    private void handleCancel() {
        settingsStage.close();
    }
}
