package gui.view;

import gui.Main;
import gui.model.DiscussionG;
import gui.model.Website;
import javafx.fxml.FXML;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;

import java.util.ArrayList;
import java.util.HashMap;

public class ThreadListController {
    @FXML
    private TableView<DiscussionG> threadTable;

    @FXML
    private TableColumn<DiscussionG, String> urlColumn;

    @FXML
    private TableColumn<DiscussionG, Number> scoreColumn;

    private Main mainApp;

    public ThreadListController() {
    }

    @FXML
    private void initialize() {
        urlColumn.setCellValueFactory(cellData -> cellData.getValue().urlProperty());
        scoreColumn.setCellValueFactory(cellData -> cellData.getValue().scoreProperty());
    }

    public void setMainApp(Main mainApp) {
        this.mainApp = mainApp;

        threadTable.setItems(mainApp.getThreadsData());
    }

    @FXML
    private void handleDeletePerson() {
        int indexOfSelectedThread = threadTable.getSelectionModel().getSelectedIndex();
        if (indexOfSelectedThread >= 0) {
            //String url = Main.threadsData.get(indexOfSelectedThread).getUrl();
            Main.threadsData.remove(indexOfSelectedThread);
            /*for (Website site : Main.websitesData) {
                for(String key : site.getThreads().keySet()){
                    if (key.equals(url)){
                        site.getThreads().remove(key);
                    }
                }
            }*/
        } else {
            System.out.println("No thread selected");
        }
    }
}
