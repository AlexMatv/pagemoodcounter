package gui.view;


import gui.Main;
import gui.model.Website;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;

import javafx.scene.chart.*;


import java.net.URL;
import java.text.DateFormatSymbols;
import java.util.*;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import javafx.scene.Scene;
import javafx.scene.chart.CategoryAxis;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import javafx.stage.Stage;

public class MoodGraphController {
    static ScheduledExecutorService scheduler = Executors.newScheduledThreadPool(1);

    @FXML
    private CategoryAxis xAxis = new CategoryAxis();

    @FXML
    private NumberAxis yAxis = new NumberAxis();

    @FXML
    private LineChart<String, Number> lineChart = new LineChart<String, Number>(xAxis, yAxis);

    @FXML
    private void initialize() {
        xAxis.setLabel("Time");
    }
        public void setData() {
            System.out.println("???");
            Runnable runnable = new Runnable() {
                public void run() {
                    try {
                        Platform.runLater(new Runnable() {
                            @Override public void run() {
                                lineChart.getData().clear();
                                for (Website website : Main.websitesData) {
                                    ArrayList<Integer> averageScores = website.calculateAverage();
                                    XYChart.Series series = new XYChart.Series();
                                    series.setName(website.getUrl());
                                    for (int i = 0; i < averageScores.size(); i++) {
                                        String xValue = ("test" + i);
                                        series.getData().add(new XYChart.Data(xValue, averageScores.get(i)));
                                    }
                                    lineChart.getData().add(series);
                                }
                            }
                        });
                    } catch (Exception e) {
                    }
                }
            };
            scheduler.scheduleAtFixedRate(runnable, 0, 10, TimeUnit.SECONDS);
        }
    }
