package gui.view;

import gui.Main;
import javafx.fxml.FXML;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import gui.Main;

public class WebsiteAddWindowController {

    @FXML
    private TextField websiteUrlField;

    private Main mainApp;

    private Stage websiteAddStage;
    private boolean addClicked = false;

    @FXML
    private void initialize(){
    }

    public void setWebsiteAddStage(Stage websiteAddStage){
        this.websiteAddStage = websiteAddStage;
    }

    public void setMainApp(Main mainApp) {
        this.mainApp = mainApp;
    }

    public boolean isAddClicked() {
        return addClicked;
    }

    @FXML
    private void handleAdd(){
        mainApp.addWebsite(websiteUrlField.getText());
        websiteUrlField.clear();
    }

    @FXML
    private void handleCancel() {
        websiteAddStage.close();
    }
}
