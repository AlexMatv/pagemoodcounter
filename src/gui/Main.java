package gui;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;

import engine.MainClass;
import gui.model.DiscussionG;
import gui.model.Website;
import gui.view.*;
import gui.view.ThreadListController;
import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.stage.Modality;
import javafx.stage.Stage;


public class Main extends Application {

    private Stage primaryStage;
    private BorderPane rootLayout;

    public static ObservableList<Website> websitesData = FXCollections.observableArrayList();
    public static ObservableList<DiscussionG> threadsData = FXCollections.observableArrayList();
    public static HashSet<String> wordValues = new HashSet<>();
    public static HashMap<String, Integer> wordValues1 = new HashMap<>();
    public static Integer checkingInterval = 20;

    public Main() {
    engine.CSVReader testFile = new engine.CSVReader();
    wordValues = testFile.getWordValues();
    for (String wordVal : wordValues) {
        String lit = wordVal.split(";")[0];
        String val = wordVal.split(";")[1];
        wordValues1.put(lit, Integer.parseInt(val));
        }
    }

    private ArrayList<String> getTestSites (ObservableList<Website> websitesData) {
        ArrayList<String> siteTest = new ArrayList<>();
        for(Website site : websitesData) {
            siteTest.add(site.getUrl());
        }
        return siteTest;
    }

    public ObservableList<DiscussionG> getThreadsData() {
        return threadsData;
    }

    @Override
    public void start(Stage primaryStage) {
        this.primaryStage = primaryStage;
        this.primaryStage.setTitle("MoodWatchWeb");
        initRootLayout();
    }

    public void initRootLayout() {
        try {
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(Main.class.getResource("view/RootLayout.fxml"));
            rootLayout = (BorderPane) loader.load();

            Scene scene = new Scene(rootLayout);
            primaryStage.setScene(scene);

            RootLayoutController controller = loader.getController();
            controller.setMain(this);

            primaryStage.show();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void showThreadList() {
        try {
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(Main.class.getResource("view/ThreadList.fxml"));
            AnchorPane threadList = (AnchorPane) loader.load();

            rootLayout.setCenter(threadList);

            ThreadListController controller = loader.getController();
            controller.setMainApp(this);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public boolean showWebsiteAddWindow() {
        try {
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(Main.class.getResource("view/WebsiteAddWindow.fxml"));
            AnchorPane page = (AnchorPane) loader.load();

            Stage websiteAddStage = new Stage();
            websiteAddStage.setTitle("Add Website");
            websiteAddStage.initModality(Modality.WINDOW_MODAL);
            websiteAddStage.initOwner(primaryStage);
            Scene scene = new Scene(page);
            websiteAddStage.setScene(scene);

            WebsiteAddWindowController controller = loader.getController();
            controller.setWebsiteAddStage(websiteAddStage);

            controller.setMainApp(this);
            websiteAddStage.showAndWait();

            return controller.isAddClicked();
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    }

    public boolean showThreadAddWindow() {
        try {
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(Main.class.getResource("view/ThreadAddWindow.fxml"));
            AnchorPane page = (AnchorPane) loader.load();

            Stage threadAddStage = new Stage();
            threadAddStage.setTitle("Add Thread");
            threadAddStage.initModality(Modality.WINDOW_MODAL);
            threadAddStage.initOwner(primaryStage);
            Scene scene = new Scene(page);
            threadAddStage.setScene(scene);

            ThreadAddWindowController controller = loader.getController();
            controller.setThreadAddStage(threadAddStage);

            controller.setMainApp(this);
            threadAddStage.showAndWait();

            return controller.isAddClicked();
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    }

    public void addWebsite(String url) {
        websitesData.add(new Website(url));
        System.out.println(websitesData);
    }

    public void addThread(String site, String page) {
        for (Website website : websitesData) {
            if (site == website.getUrl()) {
                website.addThreadurl(page);
            }
        }

        threadsData.add(new DiscussionG(page, 0));
    }

    public void showMoodGraph() {
        try {
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(Main.class.getResource("view/MoodGraph.fxml"));
        AnchorPane page = (AnchorPane) loader.load();
        Stage dialogStage = new Stage();
        dialogStage.setTitle("Mood Overview");
        dialogStage.initModality(Modality.WINDOW_MODAL);
        dialogStage.initOwner(primaryStage);
        Scene scene = new Scene(page);
        dialogStage.setScene(scene);

        MoodGraphController controller = loader.getController();
        controller.setData();

        dialogStage.show();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public boolean showSettingsWindow() {
        try {
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(Main.class.getResource("view/SettingsWindow.fxml"));
            AnchorPane page = (AnchorPane) loader.load();
            Stage settingsWindowStage = new Stage();
            settingsWindowStage.setTitle("Settings");
            settingsWindowStage.initModality(Modality.WINDOW_MODAL);
            settingsWindowStage.initOwner(primaryStage);
            Scene scene = new Scene(page);
            settingsWindowStage.setScene(scene);

            SettingsWindowController controller = loader.getController();
            controller.setSettingsStage(settingsWindowStage);

            controller.setMainApp(this);
            settingsWindowStage.showAndWait();

            return controller.isOkClicked();

        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    }

    public void startEngine(){
        ArrayList<String> siteTest = getTestSites(websitesData);
        engine.MainClass.startEngine(siteTest);
    }

    public void setInterval (Integer value){
        checkingInterval = value;
    }

    public void stopEngine(){
        engine.MainClass.stopEngine();
    }

    public static void main(String[] args) {
        launch(args);
    }
}
