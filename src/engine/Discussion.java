package engine;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;

public class  Discussion {
    private String url;

    public Integer getDicsussionScore() {
        return calculatePageScore(parcePage(url));
    }

    Discussion(String url) {
        this.url = url;
    }



    private static String parcePage(String url){
        Document doc = null;
        try {
            doc = Jsoup.connect(url).timeout(6000).ignoreHttpErrors(true).ignoreContentType(true).get();
        } catch (IOException e) {
        }
        String pageString = doc.toString();
        String noTagsString = Jsoup.parse(pageString).text();
        return noTagsString.replaceAll("[,.()!?;:\'\"]", "").toUpperCase();
    }

    private static Integer calculatePageScore(String parcedPage) {
        Integer score = 0;
        String[] pageWords = parcedPage.split(" ");
        for (String word : pageWords) {
            for(String key : gui.Main.wordValues1.keySet()) {
                int value = gui.Main.wordValues1.get(key);
                if (word.equals(key)) {
                    score += value;
                }
            }
        }
        return score;
    }

}
