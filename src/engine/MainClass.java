package engine;

import gui.Main;
import gui.model.Website;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.concurrent.*;

public class MainClass {
    private static ScheduledExecutorService scheduler = Executors.newScheduledThreadPool(10);
    public MainClass(){

    }

    public static void startEngine(ArrayList<String> sites) {

        for (String url : sites) {
            SiteProcessor site = new SiteProcessor(url);
            LinkedHashMap<String, ArrayList<Integer>> siteResults = new LinkedHashMap<>();

            Runnable runnable = new Runnable() {
                public void run() {
                    try {
                        LinkedHashMap<String, Integer> result = site.call();
                        if (siteResults.isEmpty()) {
                            for (Map.Entry<String, Integer> entry : result.entrySet()) {
                                String key = entry.getKey();
                                Integer value = entry.getValue();
                                ArrayList<Integer> list = new ArrayList<>();
                                list.add(0, value);
                                siteResults.put(key, list);
                                System.out.println(siteResults);
                            }
                        } else {
                            for (Map.Entry<String, Integer> entry : result.entrySet()){
                                String key = entry.getKey();
                                Integer value = entry.getValue();
                                siteResults.get(key).add(value);
                                System.out.println(siteResults);
                            }
                        }
                    } catch (Exception e) {
                    }

                    for (Website obsObj : gui.Main.websitesData) {
                        if (obsObj.getUrl().equals(url)) {
                            obsObj.setThreads(siteResults);
                        }
                    }
                }
            };
            scheduler.scheduleAtFixedRate(runnable, 1, Main.checkingInterval, TimeUnit.SECONDS);
        }
    }

    public static void stopEngine(){
        System.out.println("The following threadpool is being shut down: " + scheduler);
        scheduler.shutdown();
        scheduler = Executors.newScheduledThreadPool(10);
    }

}