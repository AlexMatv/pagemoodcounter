package engine;

import gui.Main;
import gui.model.Website;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.util.*;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class WebsiteEng {
    private String url;
    private  ArrayList<String> discussionsUrls;
    private LinkedHashMap<String, Integer> pagesScores;

    public LinkedHashMap<String, Integer> getPagesScores() {
        pagesScores = processSite(url);
        return pagesScores;
    }

    WebsiteEng(String url) {
        this.url = url;
        discussionsUrls = new ArrayList();
        pagesScores = new LinkedHashMap<>();
    }

    private LinkedHashMap<String, Integer> processSite (String url) {
        for (Website website : Main.websitesData) {
            if (url.equals(website.getUrl())) {
                for (String addr : website.getThreadUrls()){
                    discussionsUrls.add(addr);
                }
            }
        }
        System.out.println("Pages to parce for site:    " + url + "   is   " + discussionsUrls);

        LinkedHashMap<String, Integer> pageScores = new LinkedHashMap<>();
        ArrayList<PageProcessor> callables = new ArrayList<>();
        List<Future<String[]>> futures;

        ExecutorService executor = Executors.newCachedThreadPool();

        for(String page : discussionsUrls) {
            callables.add(new PageProcessor(page));
        }

        futures = tryInvokeCallables(callables, executor);

        for(Future<String[]> future : futures){
            String[] score = tryGetScore(future);
            Integer scoreToInteger = Integer.parseInt(score[1]);
            pageScores.put(score[0], scoreToInteger);
        }
        return pageScores;
    }

    public static List<Future<String[]>> tryInvokeCallables(ArrayList<PageProcessor> callables,
                                                            ExecutorService executor){
        List<Future<String[]>> futures = new ArrayList<>();
        try {
            futures = executor.invokeAll(callables);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return futures;
    }

    public static String[] tryGetScore(Future<String[]> future){
        String[] score = new String[0];
        try {
            score = future.get();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
        return score;
    }
}
