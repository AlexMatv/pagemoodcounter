package engine;

import gui.Main;
import gui.model.DiscussionG;

import java.util.ArrayList;
import java.util.concurrent.Callable;

public class PageProcessor implements Callable<String[]>{
    private String url;
    private String[] score;

    public PageProcessor(String url){
        this.url = url;
    }

    @Override
    public String[] call() {
        engine.Discussion discussion = new engine.Discussion(url);
        String scoreSt = String.valueOf(discussion.getDicsussionScore());
        score = new String[]{url, scoreSt};
        ArrayList<String> tempUrlList = new ArrayList<>();
        for (DiscussionG discussiong : Main.threadsData){
            tempUrlList.add(discussiong.getUrl());
        }
        for (DiscussionG discussiong : Main.threadsData){
            if (discussiong.getUrl() == url) {
                discussiong.setScore(Integer.parseInt(scoreSt));
            }
        }
        return score;
    }
}
