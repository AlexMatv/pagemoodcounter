package engine;

import java.util.LinkedHashMap;
import java.util.concurrent.Callable;

public class SiteProcessor implements Callable<LinkedHashMap<String, Integer>> {
    private String url;

    public SiteProcessor(String url){
        this.url = url;
    }

    @Override
    public LinkedHashMap<String, Integer> call() {
        WebsiteEng testSite = new WebsiteEng(url);
        return (testSite.getPagesScores());
    }
}
