package engine;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;

public class CSVReader {
    HashSet<String> wordValues;

    public HashSet<String> getWordValues() {
        return wordValues;
    }

    public CSVReader(){
        this.wordValues = readFile();
    }

    private HashSet<String> readFile() {
        String fileReference = "extfiles/WordValues.csv";
        String line = null;
        HashSet<String> wordValues = new HashSet<>();

        try (BufferedReader br = new BufferedReader(new java.io.FileReader(fileReference))) {
            while ((line = br.readLine()) != null) {
                wordValues.add(line);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return wordValues;
    }
}
